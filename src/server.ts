import Koa from 'koa'
import bodyParser from 'koa-bodyparser'

import logger from 'logger'
import config from 'config'

export default function start(): void {
  const app = new Koa()

  // Hook up middlewares
  app.use(bodyParser())

  // Setup routes
  app.use(
    async (ctx): Promise<void> => {
      ctx.body = 'hello'
    },
  )

  // Spin up!
  app.listen(
    config.PORT,
    (): void => {
      logger.log('info', `Server listening on port ${config.PORT}`)
    },
  )
}
