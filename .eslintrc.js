module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    // Disable ESLint rules from @typescript-eslint/eslint-plugin that conflicts
    // with prettier.
    'prettier/@typescript-eslint',
    // This has to be last.
    'plugin:prettier/recommended',
  ],
}
